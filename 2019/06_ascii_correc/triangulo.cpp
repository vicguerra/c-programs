#include <stdio.h>
#include <stdlib.h>

const char *simbolo = "♠";

int main (int argc, char *argv[]) {
    unsigned altura;

    // Saber cuál es la altura;
    printf ("Altura: ");
    scanf (" %u", &altura);

    // Pintar todas / cada linea;
    for (int f=0; f<altura; f++) {
       // Pintar todos / cada asterisco;
       for (int c=0; c<f+1; c++)
           printf ("%s", simbolo);
       // Imprimir un salto de linea;
       printf ("\n");
    }

    return EXIT_SUCCESS;
}
