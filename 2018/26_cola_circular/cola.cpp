#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>

#define MAX 0x04

#define RED   "\x1B[31m"
#define CYAN  "\x1B[36m"
#define RESET "\x1B[0m"

enum { no, lleno, vacio, ERRORES };
int error = vacio;

struct TCola {
    int data[MAX];
    int base;
    int cima;
};

void push (struct TCola *cola, int dato) {
    error = 0;

    if (cola->cima - cola->base >= MAX) {
        error = lleno;
        return;
    }

    cola->data[cola->cima++ % MAX] = dato;
}

int shift (struct TCola *cola) {
    error = 0;

    if (cola->cima - cola->base <= 0) {
        error = vacio;
        return error;
    }

    return cola->data[cola->base++];
}

void imprimir (struct TCola c){
    printf ("COLA\n");
    printf ("====\n");
    for (int i=c.base; i<c.cima; i++)
        printf ("\t%i\n", c.data[i % MAX]);
    printf ("\n");
}


void titulo () {
    system ("clear");
    system ("toilet -f pagga COLA");
    printf ("\n");
}

const char *vacia (int error) {
    return error == vacio ? CYAN : RESET;
}

const char *llena (int error) {
    return error == lleno ? RED : RESET;
}

int  main(int argc, char *argv[]){
    struct TCola cola;
    int nuevo,
        leidos,
        devuelto = 0,
        devuelto_temp;
    bool fin = false;
    bzero (&cola, sizeof (cola));

    do {
     titulo ();
     imprimir(cola);
     printf ("%sDevuelto: %i %s \n", vacia(error), devuelto, RESET);
     printf ("%sEntrada: %s ", llena(error), RESET);
     leidos = scanf (" %i", &nuevo);
     __fpurge(stdin);
     if (leidos)
         push (&cola, nuevo);
     else {
         devuelto_temp = shift (&cola);
         if (!error)
             devuelto = devuelto_temp;
     }
    } while (!fin);




    return EXIT_SUCCESS;
}
