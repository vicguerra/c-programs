#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>

#define RED_ON   "\x1B[31m"
#define GREEN_ON "\x1B[32m"
#define BLUE_ON  "\x1B[34m"
#define CYAN_ON  "\x1B[36m"
#define RESET    "\x1B[0m"

#define MAX 0x100
#define LMUERTE 10

bool es_mala (char letra) {
    return true;
}

int  main(int argc, char *argv[]){
    char letra;
    char *malas, *letras;
    int total_malas = 0, total_letras = 0;

    malas = (char *) malloc (LMUERTE + 1);
    letras = (char *) malloc (MAX + 1);
    bzero (malas, LMUERTE + 1);
    bzero (letras, MAX + 1);

    while (total_malas < LMUERTE){
        system ("clear");
        system ("toilet -f pagga AHOGADO");
        printf ("\n");
        printf ("Letras: " GREEN_ON "%s" RESET "\n", letras);
        printf ("Malas: " RED_ON "%s" RESET "\n", malas);
        printf ("=============================\n\n");

        printf (CYAN_ON "Letra: ");
        letra = getchar ();
        __fpurge (stdin);
        printf ( RESET "\n" );

        if (strchr (letras, letra))
            continue;

        if (es_mala (letra))
            *(malas + total_malas++) = letra;
        *(letras + total_letras++) = letra;
    }

    free (letras);

    free (malas);
    return EXIT_SUCCESS;
}
