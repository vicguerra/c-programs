#ifndef __PARRAFO_H__
#define __PARRAFO_H__

#define STRMAX 0x20

#include "string"
#include "nodo.h"

class Parrafo : public Nodo {
    public:
    int valor;

    std::string saluda ();
};

#endif
