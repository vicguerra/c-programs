#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define NS 5

#define RECUADRO "\x1B[38:5:15m\x1B[48:5:17m"
#define RESET    "\x1B[m"

const char *figura[] = {
    "Triángulo",
    "Cuadrado",
    "Rectángulo",
    "Círculo"
};

const char *simbolo[] = { "△", "▢", "▭", "○" };

enum {
    triangulo,
    cuadrado,
    rectangulo,
    circulo,
// Inserta aquí tu figura.
    FIGURAS
};

void show_menu () {
    printf ("\n"
            "\tLista de Figuras(%i)\n"
            "\t===================\n"
            "\n"
            "\t\t1. Triángulo.\n"
            "\t\t2. Cuadrado.\n"
            "\t\t3. Rectángulo.\n"
            "\t\t4. Círculo.\n"
            "\n"
            "\tOpción: ",
            FIGURAS
            );
}

void titulo_tonto (int opcion) {
    printf ("Procediendo con el:\n\t\t\t");
    for (int i=0; i<NS; i++)
        printf("%s", simbolo[opcion]);
    printf(" %s ", figura[opcion]);
    for (int i=0; i<NS; i++)
        printf("%s", simbolo[opcion]);
    printf("\n\n");
}

/* Función punto de entrada */
int  main () {
    int opcion;
    double op1, op2, area;

    system ("clear");
    system ("toilet -fpagga AREA");

    show_menu ();
    scanf (" %i", &opcion);
    system ("clear");

    titulo_tonto (--opcion);

    switch (opcion) {
        case triangulo:
            printf ("Base: ");
            scanf (" %lf", &op1);
            printf ("Altura: ");
            scanf (" %lf", &op2);
            area = op1 * op2 / 2;
            break;
        case cuadrado:
            printf ("Lado: ");
            scanf (" %lf", &op1);
            area = op1 * op1;
            break;
        case rectangulo:
            printf ("Base: ");
            scanf (" %lf", &op1);
            printf ("Altura: ");
            scanf (" %lf", &op2);
            area = op1 * op2;
            break;
        case circulo:
            printf ("Radio: ");
            scanf (" %lf", &op1);
            area = M_PI * op1 * op1;
            break;
        default:
            printf ("No sé cual es esta opción: (%i)\n", opcion + 1);
            return EXIT_FAILURE;
            break;
    }

    printf ("\n\n"
            "\t         " RECUADRO "                 "  RESET "\n"
            "\t=====>   " RECUADRO "  Área = %6.2lf  "  RESET "\n"
            "\t         " RECUADRO "                 "  RESET "\n\n", area);

    return EXIT_SUCCESS;
}
