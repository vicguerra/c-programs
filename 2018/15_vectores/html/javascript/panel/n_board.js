NBoard.id = 0
function NBoard(controller, title, parent_node) {
    this.id = NBoard.id++
    this.controller = controller
    this.title = title.humanize().capitalize()
    this.parent_node = html_node(parent_node)
    this.node = this.html_render()
    controller.panel.add(title.underscore(), this)

    this.group = {} /* Holds an html component with the name of the profile */
                    /* NBoard group share name with Nester, where every Component here 
                       maps a wizard inside Nester.                         */
}

NBoard.prototype.cast = function (mssg, data, sender) {
    switch (mssg) {
        case "activate_card":
            this.active_card = data.underscore()
            for (var i in this.group)
                this.unset_all("active")
                this.group[i].unset_all("work")
            break
        case "delete_group":
            delete this.group[data.group]
            break
        case "delete_profile":
            delete this.group[data.group].entity[data.profile]
            break
    }
    return this.controller.cast(mssg, data, sender)
  }

NBoard.prototype.add_group = function (name) {
    if (this.group[name.underscore()]) {
                alert ("Name: '" + name + "' is already taken.")
                return
    }
    this.group[name.underscore()] = new CardList(this, name)
    this.controller.cast ("group_added", name.underscore())
}

NBoard.prototype.unset_all = function(action) {
    Object.keys(this.group).forEach( (name, i) => {
        this.group[name].actuate(action, "remove")
    } )
}

NBoard.prototype.html_render = function() {
    this.node = create_node ("div", "", {style: "display: flex"}, this.parent_node)
    /* header */
    var n = create_node("div", "", {class: "ProfileGroup" }, this.node)
    create_node ("h2", this.title, {}, n)

    /**** Create Group ****/
    var m =create_node ("input", "", {type: "text", value: "Group Name" }, n)
    m.addEventListener ( "focus", function () { this.value = ''; })
    var o = create_node ("button", "New Group", {}, n)
    o.addEventListener ("click", () => {
    	this.add_group (m.value)
    } )

    /**** General Buttons ****/
    create_node("br", "", {}, n)
    var va = create_node("button", "View All", {}, n)
    va.addEventListener("click", () => { this.controller.cast("view_extent")})
    
    va = create_node("button", "Picture all", {}, n)
    va.addEventListener("click", () => { this.controller.cast("picture_all")})

    /* Cardlists */
    for (var i in this.group)
        this.node.appendChild( this.group[i].get_dom() )
    return this.node
}

NBoard.prototype.get_dom = function() {
    prune_dom(this.parent_node)
    return this.html_render()
}