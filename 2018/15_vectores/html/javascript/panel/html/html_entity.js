HTMLEntity.catalog = {
	show:   {s: "👁" 		/*"&#x1F441"*/, config: {class: "first", title: "Show in Canvas"  } },
	edit:   {s: "🖮" 		/*"&#x1F5AE"*/, config: {class: "next" , title: "Edit Coordinates"} },
	up:     {s: "⇧" 		/*"&#x21E7"*/, config: {class: "next" , title: "Punch Sooner"} },
	down:   {s: "⇩" 		/*"&#x21E9"*/, config: {class: "next" , title: "Punch Later"} },
	remove: {s: "🗶"	 	/*"&#x1F5F6"*/, config: {class: "last" , title: "Delete"} }
}
Object.defineProperty(HTMLEntity.catalog, "work", {enumerable: false})

function HTMLEntity (name, data, parent_node, card){
	this.name = name
	this.data = data
	this.parent_node = parent_node
	this.card = card
	this.action = {}
	this.action_state = {}
	Object.keys(HTMLEntity.catalog).forEach ( (action, i) => { this.action_state[action] = {class: ""} } )
	this.action_state["work"] = {class: ""}
    
    this.html_render()
}

HTMLEntity.prototype.is = function (state) {
	return this.action_state[state].class.search("Selected") != -1
}

HTMLEntity.prototype.actuate = ModuleSelect.actuate

HTMLEntity.prototype.html_render = function () {
	this.node = create_node("div", "", {class: "Profile"}, this.node)

	this.action.work = create_node("label", this.name.humanize(), { ...{style: "cursor:pointer"}, ...this.action_state["work"]}, this.node)
	this.action.work.addEventListener("click", () => { return this.do_work() })
	var k = Object.keys (HTMLEntity.catalog)
	k.forEach ( (key, i) => {
		var config = {...HTMLEntity.catalog[key].config, ...this.action_state[key]}
		config.class = (HTMLEntity.catalog[key].config.class + " " + this.action_state[key].class).trim()
		this.action[key] = create_node("button", HTMLEntity.catalog[key].s, config, this.node)
		this.action[key].addEventListener("click", (function (that) { return function (ev) { 
							return that["do_" + key].apply(that, arguments) 
						}})(this))
	})
	
	return this.node
}

HTMLEntity.prototype.get_dom = function () {
	return this.html_render()
}

HTMLEntity.prototype.do_work = function () {
	this.card.cast("work_profile", {profile: this.name.underscore(), data:this.data})
}

HTMLEntity.prototype.do_show = function () {
	this.card.cast("toggle_view_profile", {profile: this.name.underscore()})
}

HTMLEntity.prototype.do_edit = function () {
	this.card.add_profile({name: this.name.toString(), input: this.data.toSource().replace(/\[/gm, "\n[").replace(/\]\]/, "]\n]").trim() })
}


HTMLEntity.prototype.do_up = function () {
	this.card.cast("punch_sooner", {profile: this.name.underscore()})
}

HTMLEntity.prototype.do_down = function () {
	this.card.cast("punch_later", {profile: this.name.underscore()})
}

HTMLEntity.prototype.do_remove = function () {
	this.node.remove()
	this.card.cast ("delete_profile", {profile: this.name.underscore()} )
}

