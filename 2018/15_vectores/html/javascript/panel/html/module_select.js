ModuleSelect = {
	actuate: function (action, state) {
			/* state = add, remove, toggle */

			var class_list = this.action_state[action]
			switch (state) {
				case "add": 
				        if (class_list.class.search("Selected") == -1)
							class_list.class += " Selected"
					break
				case "remove": 
					if (class_list.class.search("Selected") != -1)
				        class_list.class = class_list.class.replace(" Selected", "").trim()
					break
				case "toggle": 
				        if (class_list.class.search("Selected") == -1)
							this.actuate(action, "add")
						else
							this.actuate(action, "remove")
					break
			}

		}
}