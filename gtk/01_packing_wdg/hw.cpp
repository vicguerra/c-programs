#include "gtk/gtk.h"

/* Our new imporved callback. The data passed to this function
 * is printed to stdout- */
static void callback( GtkWidget *widget,
                      gpointer data )
{
    g_print ("Hello again - %s was pressed\n", (gchar *) data);
}

/* another callback */
static gboolean delete_event( GtkWidget *widget,
                              GdkEvent  *event,
                              gpointer   data )
{
    gtk_main_quit ();
    return FALSE;
}

int main( int argc,
          char *argv[] )
{
    /* GtkWidget is the storage type for widgets */
    GtkWidget *window;
    GtkWidget *button;
    GtkWidget *box1;

    /* This is called in all GTK applications. Arguments are parsed
     * from the command line are returned to the application. */
    gtk_init (&argc, &argv);

    /* Create a new window */
    window = gtk_window_new (GTK_WINDOW_TOPLEVEL);

    /* This a new call, which just sets the title or our
     * new window to "Hello Buttons!" */
    gtk_window_set_title (GTK_WINDOW (window), "Hello Buttons!");

    /* Here we just set a handler for delete_event that inmediately
     * exits GTK. */
    g_signal_connect (window, "delete-event",
                    G_CALLBACK (delete_event), NULL);

    /* Set the border of the new window. */
    gtk_container_set_border_width (GTK_CONTAINER (window), 10);

    /* We create a box to pack widgets into. This is described in detail
     * in the "packing" section. The box is not really visible, it
     * is just used as a tool to arrange widgets. */
    box1 = gtk_hbox_new (FALSE, 0);

    /* Put the box in the main window */
    gtk_container_add (GTK_CONTAINER (window), box1);

    /* Creates a new button with the label "Button 1". */
    button = gtk_button_new_with_label ("Button 1");

    /* Now when the button is clicked, we call the "callback" function
     * with a pointer to "button 1" as its argument. " */
     g_signal_connect (button, "clicked",
                       G_CALLBACK (callback), (gpointer) "button 1");
     
     gtk_box_pack_start (GTK_BOX(box1), button, TRUE, TRUE, 0);

     /* Always remember this step, this tell GTK that our preparation for
      * this button is complete , and in cano now be displayed. */
     gtk_widget_show (button);

     /* Do these same steps again to create a second button */
     button = gtk_button_new_with_label ("Button 2");

     /* Call the same callback fucntion with a different argument,
      * passing a pointer to "button 2" instead.  */
     g_signal_connect (button, "clicked",
                       G_CALLBACK (callback), (gpointer) "button 2");

     gtk_widget_show (button);

     gtk_box_pack_start (GTK_BOX (box1), button, TRUE, TRUE, 0);

     /**The order in which we show the buttons is not really important, but I
      * recommend showing the window last, so it all pops at once. */
     gtk_widget_show (box1);
     gtk_widget_show (window);

     /* Rest in gtk_main and wait for the fun to begin! */
     gtk_main ();

     return 0;
}
