# Ejercicios de Asentamiento y Profundización en Navidades

Usa el siguiente esqueleto salvo que se te indique lo contrario.
## Esqueleto

```c
#include <stdio.h>
#include <stdlib.h>

int main () {

    return EXIT_SUCCESS;
}
```

## Problemas

1. Imprime el resto de la división: 13.0 / 7 [moldes]
2. Si un color viene definido por unsigned char r, g, b; Valores cualquiera 
definidos por el usuario. ¿Cuánto vale el XOR de r con la máscara 255? Imprime
el resultado numérico y compruébalo en un editor de fotos. ¿Y si coges otra 
máscara? Deja unos png/jpg en el directorio para ver cómo han cambiado. [Operador XOR]
3. Sea la variable de byte `unsigned waked;` que recoge cuantos miembros (papá, mama, hijo, hija)
están levantados. Se pone el bit 0(hija), 1, 2, y 3 (papá) respectivamente, se pide
completar el siguiente programa siguiendo las instrucciones proporcionadas en los comentarios.
Empieza siempre a leer los programas por la sección global, y luego sigue por el main.

```c
#include <stdio.h>
#include <stdlib.h>

#define PAPUP 8
#define MAMUP 4
#define HIJOP 2
#define HIJAP 1

enum TMiembro {
    papa,
    /* Define aquí el resto de miembros de la familia. */
    total_miembros
};


/* conmutar: devuelve el valor old_status habiéndo cambiado el valor del bit correspondiente
al miembro de la familia */
int conmutar (int old_status, enum TMiembro miembro) {
    switch (miembro) {
        /* Rellena todos los casos */
        case /*...*/
           /* Realiza la operación a nivel de bits necesaria para
           cambiar el bit correspondiente. Usa los valores en los
           define. No uses if. */
        break;
    }
}

/* acostar: devuelve el valor old_status habiéndo puesto a 0 el bit correspondiente
al miembro de la familia */
int acostar (int old_status, enum TMiembro miembro) {
    switch (miembro) {
        /* Rellena todos los casos */
        case /*...*/
           /* Realiza la operación a nivel de bits necesaria para
           poner a 0 el bit correspondiente. Usa los valores en los
           define */
        break;
    }
}

/* levantar: devuelve el valor old_status habiéndo levantado el bit correspondiente
al miembro de la familia */
int levantar (int old_status, enum TMiembro levantado) {
    switch (levantado) {
        /* Rellena todos los casos */
        case /*...*/
           /* Realiza la operación a nivel de bits necesaria para
           poner a 1 el bit correspondiente. Usa los valores en los
           define */
        break;
    }
}

/* Imprime con texto el estado de cada miembro de la familia*/
void print (int waked) {
    printf ("Papá está %s\n", /* Usa una operación de bits*/ ? "levantado": "acostado")
}

int main () {
    int waked = 0;
    enum TMiembro despertado = papa;
    
    waked = levantar (waked, papa);
    waked = acostar (waked, papa);
    waked = conmutar (waked, papa);
    
    print (waked);
    
    return EXIT_SUCCESS;
}
```



## Arrays

1. Si un color viene definido por unsigned char rgb[3]; y tenemos que:
```c
#define R 2
#define G 1
#define B 0
```
Si definimos una máscara unsigned char mask[3]; :
Eres capaz de:
    1. Pedirle al usario un color y una máscara.
    1. Hacer un XOR del color con la máscara.
    1. Dejar un ejemplo de los dos colores hecho con algún programa de dibujo.
    
2. Imprime carácter a carácter la siguiente variable.
```c
#include <stdio.h>

#define N 6

int main () {
    char nombre[N] = { 'V', 'i', 'c', 't', 'o', 'r' };
    /* Al no estar presente el valor centinela '\0' tenemos que recorrer todas
    las celdas.  */
    for (int i=0; i<N; i++)
       printf ( /*Rellena*/ );
       
    return 0;
}
```

3. Cuenta la cantidad de vocales en un nombre.
```c
#include <stdio.h>


int main () {
    char nombre[] = "Supercalifragilisticoespialidoso";
    int a, e, i, o, u;
    
    a = e = i = o = u = 0;
    
    /* Recorremos las celdas para contar la cantidad de vocales */
    for (int i=0; nombre[i] != '\0'; i++) {
       /*Rellena*/
    }
    
    /* Imprime los resultados */
    
    /* NOTA: puedes mirar el man de tolower */
       
    return 0;
}
```

4. Explícate a tí mismo por qué al preguntar el valor de nombre no hace falta
el ampersand. Házte un dibujo:

```c
    char nombre[0x20];
    printf ("Nombre: ");
    scanf (" %s", nombre);
```

5. Rellena el siguiente programa:
```c
#include <stdio.h>

#define N 20

int main () {
    int num[N];
    int acumulado = 0;
    
    for (int i=0; i<N; i++)
        /* Pon en cada celda los cuadrados de los números naturales 
        (que empiezan en 1) */
        
    /* Usando un bucle y el operador  taquigráfico += guarda en acumulado
    la suma de todas las celdas */
    
    /* Imprime el resultado */
    
    return 0;
}

```

6. Copia la constante tipo cadena `const char *origen = "Feliz Navidad";` en el
array `char destino[100];`.

7. Imprime tus 10 nombres preferidos. Usa el valor centinela para saber cuándo
has llegado al final.
```c
#include <stdio.h>

const char *nombre[] = {
    "Txema",
    /* And so on */,
    NULL
};

int main () {
  /* Todo tuyo */
}
```

## Alternativas

### If's

### Switch case

1. Dada una variable color cuyo bit 2 indica si tiene rojo, el 1 si verde y el 0 si azul, usando un switch imprime qué color 
está viendo el usuario.


## Estudio. Bucles

Mirando los apuntes intenta aproximarte a los siguientes ejercicios.

### for

El bucle for y el while son equivalentes. Sin embargo, semánticamente, el for se usa cuando se conoce
a priori el número de veces que se ejecutará el bucle.

1. Imprime 10 veces tu nombre.
2. Imprime n veces tu nombre, donde n es dado por el usuario.
3. Imprime n veces el nombre dado por el usuario.
4. Intenta usar fgets en vez de scanf para resolver el ejercio anterior.
5. Imprime c asteriscos.
6. Imprime f filas de c asteriscos. Rectñangulo
7. Introduciendo un if dentro de los bucles intenta realizar las siguientes
figuras.

```
Figura 1    Figura 2    Figura 3    Figura 5    Figura 6    Figura 7
********    *      *    ********    *                  *    ********
            *      *    *      *     *                *     **    **
            *      *    *      *      *              *      * *  * *
            *      *    *      *       *            *       *  **  *
********    *      *    ********        *          *        *  **  *
                                         *        *         * *  * *
                                          *      *          **    **
                                           *    *           ********
```

Para escurrirse el coco...
Los ejercicios para escurrirse el coco son los únicos que no es obligatorio
entregar a la vuelta de navidades.

1. Haz un caminito. Pregunta l al usuario.

```
Si l=3
***OOO***
***OOO***
***OOO***

Si l=4

****OOOO****OOOO
****OOOO****OOOO
****OOOO****OOOO
****OOOO****OOOO
```
Ten en cuenta que hay tres variables principales: fila, columna y número de 
cuadrado que estás pintando.

2. Haz un damero.
```
Si l=3

***OOO***
***OOO***
***OOO***
OOO***OOO
OOO***OOO
OOO***OOO
***OOO***
***OOO***
***OOO***
```
3. Haz una serpiente
```
Si l=3
*    **
 *  *  *
  **    *

Si l=4
*      **      *
 *    *  *    *
  *  *    *  *
   **      **
```

4. Serpiente mejorada.

```
Si l=3
*   *
 * * *
  *   *

Si l=4
*     *     *
 *   * *   *
  * *   * *
   *     *
```
Fin de para escurrirse el coco.

### while, do while

Mirando en los apuntes el funcionamiento y sintaxis de while y do ... while intenta resolver los siguientes ejercicios.
Ten en cuenta que todos los bucles ejecutan una iteración siempre que la condición sea verdadera. Nota también que
do while ejecuta la primera iteración antes de hacer la comprobación.

1. Pregunta al usuario un numero de 1 a 10 y repite la pregunta en tanto que el dato introducido no sea válido. Opcionalmente puedes
vaciar la entrada antes de preguntar mediante `__fpurge ()` incluida en `stdio_ext.h`.