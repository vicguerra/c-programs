#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define N 0x100

int  main(int argc, char *argv[]){
    char **lista;
    char buffer[N];
    int len;

    printf ("Dime tu nombre: ");
    scanf (" %s", buffer);
    len = strlen (buffer);
    lista = (char **) malloc (3 * sizeof (char *));
    lista[0] = (char *) malloc (len + 1);
    strncpy (lista[0], buffer, len + 1);
    printf ("Dime tu nombre: ");
    scanf (" %s", buffer);
    len = strlen (buffer);
    lista[1] = (char *) malloc (len + 1);
    strncpy (lista[1], buffer, len + 1);
    lista[2] = NULL;

    for (char **palabra = lista; *palabra != NULL; palabra++)
        printf ("%s\n", *palabra);

    free(lista[1]);
    free(lista[0]);
    free(lista);
    return EXIT_SUCCESS;
}
