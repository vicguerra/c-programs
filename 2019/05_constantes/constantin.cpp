#include <stdio.h>

#define EURO 166.386

#define TECNICO  1
#define SOCIALES 2
#define CONTACTO 4

enum TipoPalo {
    oros,
    copas,
    espadas,
    bastos,
    PALOS
};

int main () {
    double input;
    enum TipoPalo micarta = espadas;

    int mariano = SOCIALES | CONTACTO;

    printf ("How much do you wanna change? ");
    scanf (" %lf", &input);

    printf ("%.2lf₧ => %.2lf€\n", input, input / EURO);


    return 0;
}
