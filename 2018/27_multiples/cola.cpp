#include <stdio.h>
#include <stdio_ext.h>
#include <stdlib.h>
#include <strings.h>


#include "general.h"
#include "queue.h"
#include "gui.h"


int  main(int argc, char *argv[]){
    struct TCola cola;
    int nuevo,
        leidos,
        devuelto = 0,
        devuelto_temp;
    bool fin = false;
    bzero (&cola, sizeof (cola));

    do {
     titulo ();
     imprimir(cola);
     printf ("%sDevuelto: %i %s \n", vacia(error), devuelto, RESET);
     printf ("%sEntrada: %s ", llena(error), RESET);
     leidos = scanf (" %i", &nuevo);
     __fpurge(stdin);
     if (leidos)
         push (&cola, nuevo);
     else {
         devuelto_temp = shift (&cola);
         if (!error)
             devuelto = devuelto_temp;
     }
    } while (!fin);


    return EXIT_SUCCESS;
}
