#include <iostream>
#include <stdlib.h>
#include "parrafo.h"

using namespace std;

int
main (int argc, char *argv[])
{
    Parrafo *a = new Parrafo();

    cout << "Dime tu nombre: ";
    scanf (" %s", a->nombre);

    cout << "Dime tu valor: ";
    cin >> a->valor;

    cout << "Hola " << a->saluda() << endl;

    delete a;

    return EXIT_SUCCESS;
}
