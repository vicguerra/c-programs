#include <stdlib.h>
#include <stdio.h>

#define N 15

enum {num, den};

int main(){

    int div[2][N];
    int n_div[2] = {0, 0};
    int numerador = 78,
	denominador = 46;
    bool divisor_comun;

    do {
    /* Calculamos los divisores del primero */
    for (int pd=numerador/2; pd>1; pd--)
	if (numerador % pd == 0)
	    div[num][n_div[num]++] = pd;

    /* Calculamos los divisores del segundo */
    for (int pd=denominador/2; pd>1; pd--)
	if (denominador % pd == 0)
	    div[den][n_div[den]++] = pd;

    printf ("Fracción original: \t%i / %i\n", numerador, denominador);
    /* Buscamos un divisor común */
    divisor_comun = false;
    for (int i=0; i<n_div[num] && !divisor_comun; i++)
	for (int j=0; j<n_div[den] && !divisor_comun; j++)
	    if (div[den][j] == div[num][i]){
		numerador /= div[num][i];
		denominador/= div[num][i];
		divisor_comun = true;
		/* Si lo hay: simplificamos y volvemos a empezar */
	    }
    } while (divisor_comun == false);

    printf("Fracción simplificada: \t%i / %i", numerador, denominador);
    printf ("\n");

    return EXIT_SUCCESS;
}
