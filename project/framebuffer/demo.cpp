#include "fbd.h"
#include <math.h>

#define XC 500
#define YC 350
#define R 100

void circle (int x, int y, int r) {

    for (double a=0; a<2*M_PI; a+=0.01)
        put ( x + (int) r * cos (a), y + (int) r * sin (a), 0x80, 0x80, 0xFF, 0xFF );


}

void box (int x, int  y, int w, int h, char red, char green, char blue, char alpha) {
    for (int j=y; j<y+h; j++)
        for (int i=x; i<x+w; i++)
            put (i, j, red, green, blue, alpha);
}

int main () {
    
    open_fb ();
    info ();

    circle (XC, YC, R);
    box (10, 10, 100, 200, 0x80, 0x80, 0xFF, 0x80);
    box (50, 50, 100, 200, 0xFF, 0x80, 0x80, 0x00);

    close_fb ();

    return 0;
}
