#include <stdio.h>
#include <stdlib.h>

/* Función punto de entrada */
int  main(){
    char respuesta;

    do {
        printf ("quieres que te cuente un cuento\n"
                "recuento que nunca se acaba? (s/n)");
        scanf (" %c", &respuesta);
        printf ("\nYo no te digo ni que sí ni que no,\n"
                "lo que digo es que si ");
    } while ( respuesta != 'n' );

    printf ("Como no hay que ser un pesado en esta vida: ADIOS, Isabel.\n");

    return EXIT_SUCCESS;
}
