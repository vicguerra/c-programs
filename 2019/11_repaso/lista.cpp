#include <stdio.h>
#include <stdlib.h>

char const * lista[] = {
    "Guten Morgen",
    "Good Morning",
    "Buenos días",
    "Bon Giorno",
    "Bon Jour",
    "Bom Dia",
    "Καλημέρα",
    NULL
};

int main (int argc, char *argv[]) {
    char const **p = lista;

    while (*p != NULL){
        printf ("%s\n", *p);
        p++;
    }

    return EXIT_SUCCESS;
}
