#include <stdio.h>
#include <stdlib.h>

#define KLENTO 0.005
#define KRAPID (2 * KLENTO)
#define MAX 0x10


struct TVector {
    double x;
    double y;
};

struct TMovil {
    struct TVector pos;
    struct TVector vel;
    void (*mover) (struct TMovil * n );
};

struct TPila {
    struct TMovil *data[MAX];
    int cima;
};

void push (struct TPila *obj, struct TMovil *n) {
    if (obj->cima >= MAX)
        return;
    obj->data[obj->cima++] = n;
}

void mover_lento (struct TMovil *n) {
    n->pos.x += n->vel.x * KLENTO;
    n->pos.y += n->vel.y * KLENTO;
}

void mover_rapido (struct TMovil *n) {
    n->pos.x += n->vel.x * KRAPID;
    n->pos.y += n->vel.y * KRAPID;
}



int  main(int argc, char *argv[]){

    struct TPila personaje;

    do {
        struct TMovil *nuevo = (struct TMovil *) malloc (sizeof(struct TMovil));
        iniciar (nuevo);
        push (personaje, nuevo);

        /* Datos */
        printf ("\n");
        for (int i=0; i<personaje.cima; i++)
            printf ("Personaje %i: (X: %.2lf, Y: %.2lf)\n ", i,
                    personaje.data[i]->pos.x,  personaje.data[i]->pos.y);

    } while (1);

        for (int i=0; i<personaje.cima; i++)
            free (personaje.data[i]);

    return EXIT_SUCCESS;
}
