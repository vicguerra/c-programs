#include <stdio.h>
#include <stdlib.h>

#define VECES 33

/* Función punto de entrada */
int  main(){

    // bucle for: Debe valer para repetir cosas
    for (int i=0; i<VECES; i++) {
        printf("%2i.- Txema Escapao.\n", i);
    }

    return EXIT_SUCCESS;
}
