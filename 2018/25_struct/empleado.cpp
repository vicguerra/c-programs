#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100
#define N 0x08

struct TEmpleado {
    char nombre[MAX];
    double sueldo;
};

int  main(int argc, char *argv[]){

    struct TEmpleado empleado[N];

    /* Almacenados los datos en la variable */
    for (int i=0; i<N; i++) {
        printf ("Nombre: ");
        scanf (" %s", empleado[i].nombre);
        printf ("Sueldo: ");
        scanf (" %lf", &empleado[i].sueldo);
    }

    /* Imprimir los datos */
    for (int i=0; i<N; i++)
    printf ("%s: => %.2lf€\n",empleado[i].nombre, empleado[i].sueldo);

    return EXIT_SUCCESS;
}
