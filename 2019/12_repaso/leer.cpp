#include <stdio.h>
#include <stdlib.h>

#define MAX 0x100

int main (int argc, char *argv[]) {

    int entrada[MAX];
    int i = 0;
    bool noparar = true;

    while (noparar && i<MAX){
        scanf (" %i", &entrada[i++]);
    }

    return EXIT_SUCCESS;
}
