#include <stdio.h>
#include <stdlib.h>

int pot (int base, int exp) {
    int res = 1;
    if ( exp == 0 )
        return 1;
    res = pot (base, exp / 2);
    res = res * res;
    if (exp % 2 != 0 )
        res = res * base;
    return  res;
}
int  main(int argc, char *argv[]){

    printf ("Pot = %i\n", pot (5, 8));
    return EXIT_SUCCESS;
}
