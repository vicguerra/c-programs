#include <stdio.h>
#include <stdlib.h>

int main (int argc, char *argv[]) {

    char frase[] = "El sol esta muy alto en el cielo";


    /* Método matricial */
    int i; // Declaro i fuera del bucle para poder usar su
           // valor cuando acabe de dar vueltas
    for (i=0; frase[i]!='u' && frase[i] != '\0'; i++);
    // El bucle sólo avanza i: está vacío.


    // Cuando acaba el bucle puede ser porque hemos
    // encontrado la 'u' o porque hemos llegado al
    // final.
    if (frase[i] == 'u') {
        printf (
                "La u se encuentra a %i posiciones del inicio.\n"
                "En la dirección: %p\n", i, &frase[i]);
    }

    /* Método puntero */
    char *busca = frase;

    while (*busca != 'u' && *busca != '\0')
        busca++;


    if (*busca == 'u') {
        printf (
                "La u se encuentra a %lu posiciones del inicio.\n"
                "En la dirección: %p\n", (busca - frase) / sizeof (char), &frase[i]);
    }




    return EXIT_SUCCESS;
}
