#ifndef NAVE_H
#define NAVE_H

#include <QGraphicsPixmapItem>
#include "movil.h"

class Nave : public Movil, public QGraphicsPixmapItem
{
public:
    Nave();
    void update ();
    void draw ();
};

#endif // NAVE_H
