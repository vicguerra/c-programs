#include <stdio.h>
#include <stdlib.h>

int  main(int argc, char *argv[]){
    char *frase;
    printf ("Cuales son tus ordenes Julio? ");
    scanf (" %m[^\n]", &frase);

    for (char *p=frase; *p!='\0'; p++)
       *p += 3;

    printf ("%s\n", frase);

    free (frase);

    return EXIT_SUCCESS;
}
