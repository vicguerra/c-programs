#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QObject>
#include <QTimer>
#include <QVector>
#include "movil.h"
#include "nave.h"

#define BEAT_STEP 50

class Game : public QGraphicsView
{
    Q_OBJECT;

public:
    Game (QWidget *parent = NULL);
    int start ();

public slots:
    void run ();

private:
    QGraphicsScene *scene;
    QTimer *beat;

    QVector<Movil *> moviles;
    enum TError {
        EXIT_OK,
        ERRORS
    };
};

#endif // GAME_H
